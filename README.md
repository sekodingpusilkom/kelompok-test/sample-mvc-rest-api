# Sample MVC & REST API

[![pipeline status](https://gitlab.com/sekodingpusilkom/sample-mvc-rest-api/badges/main/pipeline.svg)](https://gitlab.com/sekodingpusilkom/sample-mvc-rest-api/-/commits/main)
[![coverage report](https://gitlab.com/sekodingpusilkom/sample-mvc-rest-api/badges/main/coverage.svg)](https://gitlab.com/sekodingpusilkom/sample-mvc-rest-api/-/commits/main)

## Pengembangan & Uji Coba Secara Lokal

1. Persiapkan database PostgresSQL secara lokal.
1. Buat Spring Profile bernama `local` dengan cara menyalin berkas konfigurasi
   `application.properties` ke berkas baru bernama `application-local.properties`
   di direktori yang sama. Kemudian isi nilai _datasource_ sesuai dengan
   konfigurasi database PostgreSQL lokal.
1. Untuk menjalankan _test suite_, panggil perintah Maven berikut:

   ```sh
   mvn test -Dspring.profiles.active=local
   ```

   > Catatan: Apabila menggunakan _shell_ PowerShell di sistem operasi Windows,
   > maka nilai pada opsi `-D` harus dikutip dalam sepasang tanda kutip (`"`).
   > Contoh: `mvn -D"spring.profiles.active=local" test`

1. Untuk menjalankan aplikasi, panggil perintah Maven berikut:

   ```sh
   mvn spring-boot:run -Dspring-boot.run.profiles=local
   ```

   > Catatan: Perhatikan bahwa nama parameter untuk menggunakan _profile_
   > berbeda pada pemanggilan _test suite_ dan aplikasi. Pastikan kembali
   > sebelum memanggil perintah Maven.

## Deployment

Silakan lihat dokumen terkait di folder [`./deploy`](./deploy/README.md).

## Lisensi dan Hak Cipta

Kode sumber ini dapat disalin, dimodifikasi, ataupun dibagi secara terbuka
selama mengikuti syarat dan ketentuan yang berlaku dalam lisensi [MIT](./LICENSE.md).

Hak Cipta (c) 2021 [Pusat Ilmu Komputer Universitas Indonesia] & [PT. Bank Sinarmas Tbk.]

[Pusat Ilmu Komputer Universitas Indonesia]: https://pusilkom.ui.ac.id
[PT. Bank Sinarmas Tbk.]: https://www.banksinarmas.com
