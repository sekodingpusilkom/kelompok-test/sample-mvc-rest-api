# Panduan Deployment

Dokumen ini menjelaskan prosedur _deployment_ komponen _backend_ dan _reverse proxy_ dari aplikasi `demo`.

## Prasyarat

Untuk dapat mengikuti dokumen ini, maka pastikan Anda memahami prasyarat berikut:

1. Ada sepasang _public-private key_ yang dibuat menggunakan OpenSSH.
   > Contoh perintah pembuatan _public-private key_: `ssh-keygen -t ed25519`
2. Ada _virtual machine_ (VM) sistem operasi Linux dengan _distro_ Ubuntu 20.04 di lingkungan _deployment_.
   > Masing-masing peserta telah memiliki sebuah VM di lingkungan _deployment_.
3. Bisa menggunakan editor teks di _command-line interface_ (CLI).
4. Bisa menggunakan _client_ FTP di CLI
   > Contoh _client_ FTP di CLI: `sftp`

## Persiapan VM

Tim operasional (Ops) seharusnya telah menanamkann _public key_ miliki peserta di VM masing-masing.
Anda bisa langsung menggunakan `ssh` untuk tersambung ke VM:

```sh
$ ssh <username>@<alamat IP VM> -p 22
```

Kemudian persiapkan _system packages_ dasar untuk dapat menjalankan _backend_ dan _reverse proxy_:

```sh
$ sudo apt-get update
$ sudo apt-get install nohup nginx openjdk11
```

Pastikan _system packages_ telah terpasang:

```sh
# Cek apakah HTTP server `nginx` telah terpasang dengan cara menguji validitas berkas konfigurasinya
$ sudo nginx -t
# Cek apakah Java 11 telah terpasang dengan cara memanggil _runtime_ Java
$ javac -version
```

Apabila _system packages_ telah terpasang, maka saatnya mempersiapkan komponen _backend_ untuk diunggah dan dijalankan di VM.

## Persiapan Deploy

Ambil berkas JAR terkini dari _pipeline_ CI yang ingin di-_deploy_. Jika menggunakan GitLab, maka berkas JAR dapat diambil dari _artifacts_ milik _pipeline_ terkait.
Kemudian, apabila berkas _artifact_ dalam bentuk ZIP, silakan ekstrak terlebih dahulu untuk memperoleh berkas JAR:

```sh
$ unzip <berkas artifact GitLab CI>.zip
```

Kemudian gunakan _client_ FTP untuk mengunggah berkas JAR ke VM:

```sh
$ sftp <username>@<alamat IP VM>
> put <nama berkas JAR>.jar
> bye
```

Dan buka kembali koneksi SSH ke VM untuk memindahkan berkas JAR ke folder _deployment_:

```sh
$ ssh <username>@<alamat IP VM> -p 22
$ mv <nama berkas JAR>.jar <path ke folder deployment>
```

Masih dalam koneksi SSH yang sama, gunakan `nohup` untuk menjalankan _backend_ sebagai _background process_:

```sh
$ nohup java -jar <nama berkas JAR>.jar
```